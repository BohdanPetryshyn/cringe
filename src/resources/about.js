import cringeImg from './images/cringe.png';
import fractalImg from './images/Infinite_Plasma_Fractal.png';
import colorModelsImg from './images/purple-fruit.jpg';
import affineTransformationsImg from './images/purple-triangle.png';
import aboutCringe from './text/aboutCringe';
import aboutFractal from './text/aboutFractal';
import aboutColorModels from './text/aboutColorModels';
import aboutAffineTransformations from './text/aboutAffinaTransformations';

export default {
  cringe: {
    title: 'Cringe',
    image: cringeImg,
    imageAlt: 'Cringe',
    text: aboutCringe,
  },
  fractal: {
    title: 'Plasma',
    image: fractalImg,
    imageAlt: 'Plasma',
    text: aboutFractal,
  },
  'color-models': {
    title: 'Color Models',
    image: colorModelsImg,
    imageAlt: 'Color Models',
    text: aboutColorModels,
  },
  'affine-transformations': {
    title: 'Affine Transformations',
    image: affineTransformationsImg,
    imageAlt: 'Affine Transformations',
    text: aboutAffineTransformations,
  }
}