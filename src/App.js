import React from 'react';
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import theme from './theme';
import SideMenu from "./components/SideMenu/SideMenu";
import Content from "./components/Content/Content";
import {Router} from 'react-router-dom'
import {createBrowserHistory} from "history";

const history = createBrowserHistory();

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Router history={history}>
        <SideMenu/>
        <Content/>
      </Router>
    </ThemeProvider>
  );
}

export default App;
export {history};