import React, {Component} from 'react';
import routes from "./routes";
import AboutResolver from "../components/AboutPage/AboutResolver";
import NotFoundErrorPage from "../components/ErrorPage/NotFoundErrorPage/NotFoundErrorPage";
import {Redirect, Route, Switch} from "react-router-dom";
import FractalPage from "../components/FractalPage/FractalPage";
import ColorModelsPage from "../components/ColorModelsPage/ColorModelsPage";
import AffineTransformationsPage from "../components/AffineTransformationsPage/AffineTransformationsPage";

class AppRouter extends Component {
  render() {

    const aboutTopicTemplateUrl = `${routes.about}/:topic`;
    const aboutCringeUrl = `${routes.about}/cringe`;

    return (
      <Switch>
        <Route exact path={routes.fractal} component={FractalPage}/>
        <Route exact path={routes.colorModels} component={ColorModelsPage}/>
        <Route exact path={routes.affineTransformations} component={AffineTransformationsPage}/>
        <Route path={aboutTopicTemplateUrl} component={AboutResolver}/>
        <Redirect exact from='/' to={aboutCringeUrl}/>
        <Route path="/" component={NotFoundErrorPage}/>
      </Switch>
    )
  }
}

export default AppRouter;