export default {
  fractal: '/fractal',
  colorModels: '/color-models',
  affineTransformations: '/affine-transformations',
  cringe: '/cringe',
  about: '/about',
}