import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import {grey, indigo} from "@material-ui/core/colors";

export default createMuiTheme({
  palette: {
    primary: {
      main: indigo[300]
    },
    secondary: {
      main: grey[800]
    },
  },
  sideMenuWidth: 270,
});