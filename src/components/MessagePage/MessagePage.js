import React, {Component} from 'react';
import Typography from "@material-ui/core/Typography";
import {withStyles} from "@material-ui/styles";
import Grid from "@material-ui/core/Grid";

class MessagePage extends Component {
  render() {
    const {classes} = this.props;
    return (
      <Grid
        container
        direction="column"
        justify="center"
        alignItems="center"
        className={classes.grid}
      >
        <Grid item>
          <Typography variant="h3" className={classes.message}>
            {this.props.message}
          </Typography>
        </Grid>
      </Grid>
    )
  }
}

const styles = {
  message: {
    color: "#ffffff"
  },
  grid: {
    height: "80vh"
  }
};

export default withStyles(styles)(MessagePage);