import React, {Component} from 'react';
import MessagePage from "../../MessagePage/MessagePage";

class NotFoundErrorPage extends Component {
  render() {
    return <MessagePage message="Cringe! Such page doesn't exist"/>;
  }
}

export default NotFoundErrorPage;