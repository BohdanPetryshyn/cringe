import React, {Component} from 'react';
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import {Link} from "react-router-dom";
import withStyles from "@material-ui/styles/withStyles/withStyles";

class HelpLink extends Component {
  render() {
    const {classes} = this.props;

    return (
      <Grid container justify="center">
        <Grid item>
          <Link
            underline="always"
            to={this.props.to}
            className={classes.link}
          >
            <Typography variant="caption">
              {this.props.caption}
            </Typography>
          </Link>
        </Grid>
      </Grid>
    )
  }
}

const styles = theme => ({
  link: {
    color: theme.palette.primary.main,
  },
});

export default withStyles(styles, {withTheme: true})(HelpLink);