import React, {Component} from 'react';
import ArrowLeftIcon from '@material-ui/icons/ArrowLeft';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import {withStyles} from "@material-ui/styles";

class VertexPicker extends Component {
  render() {
    const {classes} = this.props;
    return (
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
        spacing={2}
      >
        <Grid item>
          <IconButton
            size="small"
            className={classes.button}
            onClick={() => this.props.prevClick()}
          >
            <ArrowLeftIcon/>
          </IconButton>
        </Grid>
        <Grid item>
          <Typography variant='body2' className={classes.text}>
            Pick Vertex To Rotate Around
          </Typography>
        </Grid>
        <Grid item>
          <IconButton
            size="small"
            className={classes.button}
            onClick={() => this.props.nextClick()}
          >
            <ArrowRightIcon/>
          </IconButton>
        </Grid>
      </Grid>
    );
  }
}

const styles = theme => ({
  button: {
    background: theme.palette.primary.main,
    color: 'inherit',
  },
  text: {
    marginTop: '5px'
  }

});

export default withStyles(styles, {withTheme: true})(VertexPicker);