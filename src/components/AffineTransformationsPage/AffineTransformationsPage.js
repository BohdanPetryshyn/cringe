import React, {Component} from 'react';
import Grid from "@material-ui/core/Grid";
import AffineTransformationControls from "./AffineTransformationControls";
import TriangleView from "./TriangleView";
import {compareCoords} from "../../utils/coordinateUtils";
import {
  combineTransformations,
  rotateTransformation,
  scaleTransformation,
  transformPointWithCenter,
  transformShapeWithCenter
} from "../../utils/affineTransformations";

const SCALE_MULTIPLIER = 0.02;
const ANGLE_MULTIPLIER = Math.PI * 2 / 100;
const DEFAULT_SCALE = 1;
const DEFAULT_ANGLE = 0;

class AffineTransformationsPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      triangle: {
        a: [300, 300],
        b: [355, 210],
        c: [410, 300],
      },
      center: [300, 300],
      angle: DEFAULT_ANGLE,
      scale: DEFAULT_SCALE,
    }
  }

  resetWithNewCenter = (prevState, newCenter) => {
    const {triangle, angle, scale, center} = prevState;

    const transformedTriangle = transformShapeWithCenter(
      triangle,
      center,
      combineTransformations(
        scaleTransformation(scale),
        rotateTransformation(angle)
      ));
    const transformedCenter = transformPointWithCenter(
      newCenter,
      center,
      combineTransformations(
        scaleTransformation(scale),
        rotateTransformation(angle)
      ));
    return {
      triangle: transformedTriangle,
      center: transformedCenter,
      angle: DEFAULT_ANGLE,
      scale: DEFAULT_SCALE,
    }
  };

  handleNextVertexClick = () => {
    this.setState(prevState => {
      const {
        triangle: {a, b, c},
        center
      } = prevState;

      if (compareCoords(center, a)) return this.resetWithNewCenter(prevState, b);
      if (compareCoords(center, b)) return this.resetWithNewCenter(prevState, c);
      if (compareCoords(center, c)) return this.resetWithNewCenter(prevState, a);
    })
  };

  handlePrevVertexClick = () => {
    this.setState(prevState => {
      const {
        triangle: {a, b, c},
        center
      } = prevState;
      if (compareCoords(center, a)) return this.resetWithNewCenter(prevState, c);
      if (compareCoords(center, b)) return this.resetWithNewCenter(prevState, a);
      if (compareCoords(center, c)) return this.resetWithNewCenter(prevState, b);
    })
  };

  handleScaleChange = scale => {
    this.setState({scale: scale * SCALE_MULTIPLIER});
  };

  handleAngleChange = angle => {
    this.setState({angle: angle * ANGLE_MULTIPLIER});
  };

  render() {
    const {
      triangle,
      scale,
      angle,
      center
    } = this.state;
    const transformedTriangle = transformShapeWithCenter(
      triangle,
      center,
      combineTransformations(
        scaleTransformation(scale),
        rotateTransformation(angle)
      ));

    return (
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="flex-start"
      >
        <Grid item xs={9}>
          <TriangleView
            width={600}
            height={600}
            triangle={transformedTriangle}
            dot={center}/>
        </Grid>
        <Grid item xs={3}>
          <AffineTransformationControls
            onNextVertexClick={this.handleNextVertexClick}
            onPrevVertexClick={this.handlePrevVertexClick}
            scale={scale / SCALE_MULTIPLIER}
            angle={angle / ANGLE_MULTIPLIER}
            onScaleChange={this.handleScaleChange}
            onAngleChange={this.handleAngleChange}
          />
        </Grid>
      </Grid>
    )
  }
}

export default AffineTransformationsPage;

