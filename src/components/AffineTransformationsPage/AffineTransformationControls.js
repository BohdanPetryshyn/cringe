import React, {Component} from 'react';
import Grid from "@material-ui/core/Grid";
import VertexPicker from "./VertexPicker";
import AnglePicker from "./AnglePicker";
import routes from "../../router/routes";
import HelpLink from "../HelpLink/HelpLink";
import {withStyles} from "@material-ui/styles";
import Typography from "@material-ui/core/Typography";
import Slider from "@material-ui/core/Slider";

class AffineTransformationControls extends Component {
  render() {
    const {classes} = this.props;
    return (
      <Grid
        container
        direction="column"
        justify="flex-start"
        alignItems="flex-start"
        spacing={3}
      >
        <Grid item>
          <VertexPicker
            nextClick={this.props.onNextVertexClick}
            prevClick={this.props.onPrevVertexClick}
          />
        </Grid>
        <Grid item>
          <AnglePicker
            onAngleChange={this.props.onAngleChange}
            angle={this.props.angle}
          />
        </Grid>
        <Grid item>
          <Typography id="scale-slider" variant="body2" gutterBottom>
            Scale
          </Typography>
          <Slider
            className={classes.scaleSlider}
            area-labelledby={'scale-slider'}
            defaultValue={50}
            onChange={(__, value) => this.props.onScaleChange(value)}
            value={this.props.scale}
          />
        </Grid>
        <Grid item>
          <HelpLink
            caption="What is affine transformations?"
            to={`${routes.about}${routes.affineTransformations}`}
          />
        </Grid>

      </Grid>
    )
  }
}

const styles = {
  scaleSlider: {
    width: 280
  }
};

export default withStyles(styles)(AffineTransformationControls);