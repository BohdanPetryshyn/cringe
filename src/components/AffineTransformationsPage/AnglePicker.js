import React, {Component} from 'react';
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Slider from "@material-ui/core/Slider";
import Input from "@material-ui/core/Input";
import {withStyles} from "@material-ui/styles";

const SLIDER_INPUT_MULTIPLIER = 3.6;

class AnglePicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    return {value: nextProps.angle * SLIDER_INPUT_MULTIPLIER};
  }

  handleSliderChange = (event, value) => {
    const scaledValue = value * SLIDER_INPUT_MULTIPLIER;
    this.setState({value: scaledValue});
    this.props.onAngleChange(value);
  };

  getSliderValue() {
    return this.state.value / SLIDER_INPUT_MULTIPLIER;
  }

  handleInputChange = event => {
    const value = event.target.value === '' ? 0 : Number(event.target.value);
    if (value >= 0 && value <= 360) {
      this.setState({value});
      this.props.onAngleChange(value / SLIDER_INPUT_MULTIPLIER);
    }
  };

  render() {
    const value = this.state.value;
    const {classes} = this.props;
    return (
      <>
        <Typography variant="body2" id="input-slider" gutterBottom>
          Angle
        </Typography>
        <Grid container spacing={1} alignItems="center" justify="center">
          <Grid item>
            <Slider
              value={this.getSliderValue()}
              onChange={this.handleSliderChange}
              className={classes.slider}
              aria-labelledby="input-slider"
            />
          </Grid>
          <Grid item>
            <Input
              value={value}
              onChange={this.handleInputChange}
              className={classes.input}
            />
          </Grid>
        </Grid>
      </>
    )
  }
}

const styles = {
  input: {
    width: 60,
    color: 'inherit'
  },
  slider: {
    width: 180,
  }
};

export default withStyles(styles, {withTheme: true})(AnglePicker);