import React, {Component} from 'react';
import {indigo} from "@material-ui/core/colors";

class TriangleView extends Component {
  constructor(props) {
    super(props);
    this.canvas = React.createRef();
  }

  componentDidMount() {
    this.updateCanvas();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    this.updateCanvas();
  }

  updateCanvas() {
    const ctx = this.canvas.current.getContext('2d');
    const {
      triangle: {
        a: [aX, aY],
        b: [bX, bY],
        c: [cX, cY],
      },
      dot: [dotX, dotY],
    } = this.props;

    this.clearCanvas();

    ctx.beginPath();
    ctx.moveTo(aX, aY);
    ctx.lineTo(bX, bY);
    ctx.lineTo(cX, cY);
    ctx.closePath();

    ctx.lineWidth = 5;
    ctx.strokeStyle = '#666666';
    ctx.stroke();

    ctx.fillStyle = indigo[300];
    ctx.fill();

    ctx.beginPath();
    ctx.arc(dotX, dotY, 10, 0, 2 * Math.PI);
    ctx.fillStyle = indigo[400];
    ctx.fill();
  }

  clearCanvas() {
    const {width, height} = this.props;
    const ctx = this.canvas.current.getContext('2d');
    ctx.clearRect(0, 0, width, height);
  }

  render() {
    const {width, height} = this.props;
    return (
      <canvas ref={this.canvas} width={width} height={height}/>
    )
  }
}

export default TriangleView;