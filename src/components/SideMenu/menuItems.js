import AcUnitIcon from '@material-ui/icons/AcUnit';
import CompareIcon from '@material-ui/icons/Compare';
import BubbleChartIcon from '@material-ui/icons/BubbleChart';
import InfoIcon from '@material-ui/icons/Info';
import routes from "../../router/routes";

export default {
  fractal: {
    text: 'Fractal',
    icon: AcUnitIcon,
    link: routes.fractal,
  },
  colorModels: {
    text: 'Color models',
    icon: CompareIcon,
    link: routes.colorModels,
  },
  affineTransformations: {
    text: 'Affine Transformations',
    icon: BubbleChartIcon,
    link: routes.affineTransformations
  },
  aboutCringe: {
    text: 'AboutCringe',
    icon: InfoIcon,
    link: `${routes.about}${routes.cringe}`
  }
}