import React, {Component} from 'react';
import Drawer from "@material-ui/core/Drawer";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import menuItems from "./menuItems";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import withStyles from "@material-ui/styles/withStyles";
import {NavLink, withRouter} from "react-router-dom";

class SideMenu extends Component {
  createIcon(element) {
    const {classes} = this.props;
    return React.createElement(element, {className: classes.icon});
  }

  render() {
    const {classes} = this.props;

    return (
      <Drawer variant="permanent" className={classes.drawer} classes={{paper: classes.paper}}>
        <Typography variant="h3" className={classes.menuTitle}>
          Cringe
        </Typography>
        <Divider variant="middle" className={classes.divider}/>
        <List>
          {Object.entries(menuItems).map(([key, value]) => (
            <ListItem button key={key} component={NavLink} to={value.link} activeClassName={classes.active}>
              <ListItemIcon> {this.createIcon(value.icon)} </ListItemIcon>
              <ListItemText primary={value.text}/>
            </ListItem>
          ))}
        </List>
      </Drawer>
    );
  }
}

const styles = theme => ({
  drawer: {
    width: theme.sideMenuWidth,
  },
  paper: {
    background: theme.palette.primary.main,
    color: '#ffffff',
    width: theme.sideMenuWidth,
  },
  icon: {
    color: "#ffffff"
  },
  menuTitle: {
    margin: theme.spacing(2)
  },
  divider: {
    color: "#ffffff",
    height: "2px"
  },
  active: {
    backgroundColor: 'rgba(0, 0, 0, 0.11)',
  },
});

export default withRouter(withStyles(styles, {withTheme: true})(SideMenu));