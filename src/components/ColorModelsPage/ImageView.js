import React, { Component } from 'react';
import {debounce} from 'lodash';
import {withYellowMultiplier} from "../../utils/imageUtils";

const MAX_YELLOW_MULTIPLIER = 2;
const RENDER_DEBOUNCE_TIME = 200;

class ImageView extends Component {
  constructor(props) {
    super(props);
    this.canvas = React.createRef();
    this.updateCanvas = debounce(
      this.updateCanvas,
      RENDER_DEBOUNCE_TIME
    )
  }

  getMousePos = (e) => {
    const rect = this.canvas.current.getBoundingClientRect();
    return {
      x: e.clientX - rect.left,
      y: e.clientY - rect.top
    };
  };

  componentDidMount() {
    this.canvas.current.addEventListener('mousemove', (e) => {
      this.props.onMouseMove(this.getMousePos(e))
    })
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    this.updateCanvas();
  }

  handleChange() {
    const dataURL = this.canvas.current.toDataURL('image/png');
    this.props.onChange( dataURL);
  }

  updateCanvas() {
    const ctx = this.canvas.current.getContext('2d');
    const yellowMultiplier = this.props.yellowLevel * MAX_YELLOW_MULTIPLIER / 100;
    const imageData = withYellowMultiplier(ctx, this.props.imageData, yellowMultiplier);
    ctx.putImageData(imageData, 0, 0);
    this.handleChange();
  }

  render() {
    const {imageData: {height, width}} = this.props;
    return (
      <>
        <canvas ref={this.canvas} width={width} height={height}/>
      </>
    );
  }
}

export default ImageView;