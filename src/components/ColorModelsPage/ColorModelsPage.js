import React, {Component} from 'react';
import Grid from "@material-ui/core/Grid";
import ColorModelsControls from "./ColorModelsControls";
import ImageView from "./ImageView";
import ColorInfo from "./ColorInfo";
import {getRgbPixelFromImageData} from "../../utils/imageUtils";

class ColorModelsPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      yellowLevel: 50,
      imageData: null,
      imageUrl: null,
      mousePos: {
        x: 0,
        y: 0
      },
    }
  }

  handleYellowLevelChange = yellowLevel => {
    this.setState({yellowLevel});
  };

  handleImageSelected = imageData => {
    this.setState({imageData});
    this.forceUpdate();
  };

  handleMousePosChange = mousePos => {
    this.setState({mousePos});
  };

  handleViewChange = imageUrl => {
    this.setState({imageUrl})
  };

  renderImageView() {
    const {imageData, yellowLevel} = this.state;
    return (
      <>
        {imageData &&
        <ImageView
          onChange={this.handleViewChange}
          imageData={imageData}
          yellowLevel={yellowLevel}
          onMouseMove={this.handleMousePosChange}
        />}
      </>
    )
  }

  renderColorInfo() {
    const {imageData} = this.state;
    const {x, y} = this.state.mousePos;
    return (
      <>
        {imageData &&
        <ColorInfo color={getRgbPixelFromImageData(imageData, x, y)}/>
        }
      </>
    )
  }

  render() {
    return (
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="flex-start"
      >
        <Grid item xs={9}>
          {this.renderImageView()}
          {this.renderColorInfo()}
        </Grid>
        <Grid item xs={3}>
          <ColorModelsControls
            onImageSelected={this.handleImageSelected}
            onYellowLevelChange={this.handleYellowLevelChange}
            imageUrl={this.state.imageUrl}
            yellowLevelValue={this.state.yellowLevel}
          />
        </Grid>
      </Grid>
    )
  }
}

export default ColorModelsPage;