import React, { Component } from 'react';
import {Typography} from "@material-ui/core";
import {rgbToCmyk} from "../../utils/colorUtils";

class ColorInfo extends Component {
  render() {
    const { r, g, b} = this.props.color;
    const {c, m, y, k} = rgbToCmyk(this.props.color);
    return (
      <>
        <Typography variant="body2">
          {`RGB(${r}, ${g}, ${b})`}
        </Typography>
        <Typography variant="body2">
          {`CMYK(${c.toFixed(2)}, ${m.toFixed(2)}, ${y.toFixed(2)}, ${k.toFixed(2)})`}
        </Typography>
      </>
    );
  }
}

export default ColorInfo;