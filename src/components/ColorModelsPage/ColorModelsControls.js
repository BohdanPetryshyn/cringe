import React, {Component} from 'react';
import AppButton from "../AppButton/AppButton";
import HelpLink from "../HelpLink/HelpLink";
import routes from "../../router/routes";
import Typography from "@material-ui/core/Typography";
import Slider from "@material-ui/core/Slider";
import ImageSelector from "./ImageSelector";

class ColorModelsControls extends Component {

  handleYellowLevelChange = (e, value) => {
    this.props.onYellowLevelChange(value);
  };

  renderYellowLevelPicker() {
    return (
      <>
        <Typography id="slider" variant="body2" gutterBottom>
          Yellow Level
        </Typography>
        <Slider
          area-labelledby={'slider'}
          value={this.props.yellowLevelValue}
          onChange={this.handleYellowLevelChange}
        />
      </>
    )
  }

  renderDownloadResultButton() {
    const { imageUrl } = this.props;
    return (

      <AppButton
        disabled={!imageUrl}
        href={imageUrl}
        text="Save Result"
        download="result.png"
      />
    )
  }

  render() {
    return (
      <>
        <ImageSelector onImageSelected={this.props.onImageSelected}/>
        {this.renderYellowLevelPicker()} <br/>
        {this.renderDownloadResultButton()}<br/>
        <HelpLink caption="What is color models?" to={`${routes.about}${routes.colorModels}`}/>
      </>
    )
  }
}

export default ColorModelsControls;