import React, {Component} from 'react';
import AppButton from "../AppButton/AppButton";
import {base64UrlToImageData} from "../../utils/imageUtils";

class ImageSelector extends Component {
  constructor(props) {
    super(props);
    this.fileReader = new FileReader();
  }

  handleImageRead = () => {
    base64UrlToImageData(this.fileReader.result)
      .then(imageData => this.props.onImageSelected(imageData))
  };

  handleFileSelected = file => {
    this.fileReader.onloadend = this.handleImageRead;
    this.fileReader.readAsDataURL(file);
  };

  render() {
    return (
      <>
        <input
          accept="image/*"
          style={{display: 'none'}}
          id="raised-button-file"
          type="file"
          onChange={e => this.handleFileSelected(e.target.files[0])}
        />
        <label htmlFor="raised-button-file">
          <AppButton variant="raised" component="span" text="Chose Image"/>
        </label>
      </>
    );
  }
}

export default ImageSelector;