import React, {Component} from 'react';
import HelpLink from "../HelpLink/HelpLink";
import routes from "../../router/routes";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import {SliderPicker} from "react-color";
import Slider from "@material-ui/core/Slider";
import {withStyles} from "@material-ui/styles";

const ASPECT_RATION_MULTIPLIER = 0.01;
const ASPECT_RATIO_SHIFT = 0.5;

class FractalControls extends Component {

  getAspectRatioValue() {
    return (this.props.aspectRatioValue - ASPECT_RATIO_SHIFT) / ASPECT_RATION_MULTIPLIER;
  }

  renderColorPicker(title, propName) {
    const changePropName = `${propName}Change`;
    const valuePropName = `${propName}Value`;

    return (
      <>
        <Typography
          id={title}
          variant="body2"
          gutterBottom>
          {title}
        </Typography>
        <SliderPicker
          color={this.props[valuePropName]}
          onChangeComplete={c => this.props[changePropName](c.rgb)}
          area-labelledby={title}/>
      </>
    )
  }

  renderRatioPicker() {
    return (
      <>
        <Typography id="slider" variant="body2" gutterBottom>
          Aspect Ratio
        </Typography>
        <Slider
          area-labelledby={'slider'}
          defaultValue={this.getAspectRatioValue()}
          onChange={(e, value) =>
            this.props.aspectRatioChange(value * ASPECT_RATION_MULTIPLIER + ASPECT_RATIO_SHIFT)}
        />
      </>
    )
  }

  render() {
    const {classes} = this.props;
    return (
      <Grid
        container
        direction="column"
        justify="flex-start"
        alignItems="flex-start"
        spacing={3}
      >
        <Grid item className={classes.gridItem}>
          {this.renderRatioPicker()}
        </Grid>
        <Grid item className={classes.gridItem}>
          {this.renderColorPicker(
            'Upper-Left Corner Color',
            'upperLeftColor'
          )}
        </Grid>
        <Grid item className={classes.gridItem}>
          {this.renderColorPicker(
            'Down-Left Corner Color',
            'upperRightColor'
          )}
        </Grid>
        <Grid item className={classes.gridItem}>
          {this.renderColorPicker(
            'Upper-Right Corner Color',
            'downLeftColor'
          )}
        </Grid>
        <Grid item className={classes.gridItem}>
          {this.renderColorPicker(
            'Down-Right Corner Color',
            'downRightColor'
          )}
        </Grid>
        <Grid item className={classes.gridItem}>
          <HelpLink caption="What is fractal?" to={`${routes.about}${routes.fractal}`}/>
        </Grid>
      </Grid>
    )
  }
}

const styles = {
  gridItem: {
    minWidth: '100%',
  }
};

export default withStyles(styles)(FractalControls);
