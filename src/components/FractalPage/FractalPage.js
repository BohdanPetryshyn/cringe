import React, {Component} from 'react';
import {debounce} from 'lodash';
import Grid from "@material-ui/core/Grid";
import FractalControls from "./FractalControls";
import Plasma from "./Plasma";

const RED = {r: 256, g: 0, b: 0, a: 256};
const BLACK = {r: 0, g: 0, b: 0, a: 256};

const WIDTH = 600;

const DEBOUNCE_TIME = 1500;

class FractalPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      upperLeftColor: BLACK,
      upperRightColor: BLACK,
      downRightColor: RED,
      downLeftColor: BLACK,
      aspectRatio: 1,
    }
  }

  handleStateChange(propName) {
    return debounce(val => {
      this.setState({
        [propName]: val,
      });
    }, DEBOUNCE_TIME)
  }

  renderPlasma() {
    const {
      upperLeftColor,
      upperRightColor,
      downRightColor,
      downLeftColor,
      aspectRatio,
    } = this.state;

    const height = Math.floor(WIDTH * aspectRatio);
    return (
      <Plasma
        width={WIDTH}
        height={height}
        upperLeftColor={upperLeftColor}
        upperRightColor={upperRightColor}
        downRightColor={downRightColor}
        downLeftColor={downLeftColor}
      />
    )
  }

  renderFractalControls() {
    const {
      upperLeftColor,
      upperRightColor,
      downRightColor,
      downLeftColor,
      aspectRatio,
    } = this.state;
    return (
      <FractalControls
        upperLeftColorChange={this.handleStateChange('upperLeftColor')}
        upperRightColorChange={this.handleStateChange('upperRightColor')}
        downLeftColorChange={this.handleStateChange('downLeftColor')}
        downRightColorChange={this.handleStateChange('downRightColor')}
        aspectRatioChange={this.handleStateChange('aspectRatio')}
        upperLeftColorValue={upperLeftColor}
        upperRightColorValue={upperRightColor}
        downRightColorValue={downRightColor}
        downLeftColorValue={downLeftColor}
        aspectRatioValue={aspectRatio}
      />
    )
  }

  render() {
    return (
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="flex-start"
      >
        <Grid item xs={9}>
          {this.renderPlasma()}
        </Grid>
        <Grid item xs={3}>
          {this.renderFractalControls()}
        </Grid>
      </Grid>
    )
  }
}

export default FractalPage;