import React, {Component} from 'react';
import {plasma} from "../../utils/plasma";
import {pixelMatrixToRgbaArray} from "../../utils/imageUtils";


class Plasma extends Component {
  constructor(props) {
    super(props);
    this.canvas = React.createRef();
    this.state = {
      rendering: true,
    }
  }

  componentDidMount() {
    this.updateCanvas();
  }

  componentDidUpdate() {
    this.updateCanvas();
  }

  updateCanvas() {
    const {
      width,
      height,
      upperLeftColor,
      upperRightColor,
      downRightColor,
      downLeftColor,
    } = this.props;

    const ctx = this.canvas.current.getContext('2d');

    const plasmaPixels = plasma(
      width,
      height,
      upperLeftColor,
      upperRightColor,
      downRightColor,
      downLeftColor,
      250,
      1.5
    );

    const imageData = ctx.createImageData(width, height);
    imageData.data.set(new Uint8ClampedArray(pixelMatrixToRgbaArray(plasmaPixels)));

    ctx.putImageData(imageData, 0, 0);
  }

  render() {
    const {width, height} = this.props;
    return (
      <canvas ref={this.canvas} width={width} height={height}/>
    )
  }
}

export default Plasma;