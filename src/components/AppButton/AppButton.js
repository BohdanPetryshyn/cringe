import React, {Component} from 'react';
import Button from "@material-ui/core/Button";
import {withStyles} from "@material-ui/styles";
import Typography from "@material-ui/core/Typography";

class AppButton extends Component {
  render() {
    const {classes, text, ...restProps} = this.props;
    return (
      <Button className={classes.button} {...restProps}>
        <Typography variant='body2'>
          {text}
        </Typography>
      </Button>
    )
  }
}

const styles = theme => ({
  button: {
    background: theme.palette.primary.main,
    color: 'inherit',
    width: '100%',
    marginTop: '10px',
    marginBottom: '15px'
  }
});

export default withStyles(styles, {withTheme: true})(AppButton);