import React, {Component} from 'react';
import {withStyles} from "@material-ui/styles";
import Paper from "@material-ui/core/Paper";
import AppRouter from "../../router/AppRouter";

class Content extends Component {
  render() {
    const {classes} = this.props;
    return (
      <Paper className={classes.root}>
        <AppRouter/>
      </Paper>
    )
  }
}

const styles = theme => ({
  root: {
    margin: theme.spacing(2),
    marginLeft: `${theme.sideMenuWidth + theme.spacing(2)}px`,
    padding: theme.spacing(3),
    minHeight: `calc(100vh - ${theme.spacing(10)}px)`,
    background: theme.palette.secondary.main,
    position: "relative",
    color: "#ffffff",
  }
});

export default withStyles(styles)(Content);