import React, {Component} from 'react';
import about from '../../resources/about'
import AboutPage from "./AboutPage";
import NotFoundErrorPage from "../ErrorPage/NotFoundErrorPage/NotFoundErrorPage";

class AboutResolver extends Component {
  render() {
    const topic = this.props.match.params.topic;
    if (!about[topic]) return <NotFoundErrorPage/>;
    return (
      <AboutPage {...about[topic]} />
    )
  }
}

export default AboutResolver;