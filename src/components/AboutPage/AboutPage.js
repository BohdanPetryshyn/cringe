import React, {Component} from 'react';
import Typography from "@material-ui/core/Typography";
import {withStyles} from "@material-ui/styles";

class AboutPage extends Component {
  render() {
    const {classes} = this.props;

    return (
      <>
        <Typography variant="h3" className={classes.text}>
          {this.props.title}
        </Typography>
        <img src={this.props.image} alt={this.props.imageAlt} className={classes.image}/>
        <Typography variant="body1" className={classes.text}>
          {this.props.text}
        </Typography>
      </>
    );
  }
}

const styles = theme => ({
  text: {
    margin: theme.spacing(2),
    color: "#ffffff"
  },
  image: {
    float: "left",
    margin: theme.spacing(2),
    width: '30%',
    borderRadius: '4px'
  }
});

export default withStyles(styles)(AboutPage);