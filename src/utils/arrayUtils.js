export const array2D = (width, height) => {
  const array = new Array(width);
  for (let i = 0; i < width; i++) {
    array[i] = new Array(height);
  }
  return array;
};