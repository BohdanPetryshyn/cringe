const ALPHA_MULTIPLIER = 256;

const COMPONENTS_IN_PIXEL = 4;
const RED_COMPONENT_INDEX = 0;
const GREEN_COMPONENT_INDEX = 1;
const BLUE_COMPONENT_INDEX = 2;

export const pixelMatrixToRgbaArray = pixels => {
  return pixels
    .flat()
    .flatMap(pixel => [
      pixel.r,
      pixel.g,
      pixel.b,
      pixel.a * ALPHA_MULTIPLIER
    ]);
};

export const base64UrlToImageData = url => {
  return new Promise(resolve => {
    const image = new Image();
    image.onload = function() {
      const canvas = document.createElement('canvas');
      canvas.width = image.width;
      canvas.height = image.height;

      const context = canvas.getContext('2d');
      context.drawImage(image, 0, 0);

      const imageData = context.getImageData(0, 0, canvas.width, canvas.height);
      resolve(imageData);
    };
    image.src = url;
  })
};

export const withYellowMultiplier = (ctx, imageData, multiplier) => {
  const result = ctx.createImageData(imageData.width, imageData.height);
  result.data.set(imageData.data.map((component, i) => {
    const pixelComponentNumber = i % COMPONENTS_IN_PIXEL;
    if(pixelComponentNumber === RED_COMPONENT_INDEX
      || pixelComponentNumber === GREEN_COMPONENT_INDEX) {
      return component * multiplier;
    }
    return component;
  }));
  return result;
};

export const getRgbPixelFromImageData = (imageData, x, y) => {
  const linearPixelIndex = imageData.width * y + x;
  const linearPixelComponentIndex = linearPixelIndex * COMPONENTS_IN_PIXEL;
  const pixelData = imageData.data.slice(
    linearPixelComponentIndex,
    linearPixelComponentIndex + COMPONENTS_IN_PIXEL);
  return({
    r: pixelData[RED_COMPONENT_INDEX],
    g: pixelData[GREEN_COMPONENT_INDEX],
    b: pixelData[BLUE_COMPONENT_INDEX]
  })
};
