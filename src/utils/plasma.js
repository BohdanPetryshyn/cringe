import {array2D} from "./arrayUtils";

export const plasmaPromise = (...props) => {
  return new Promise(resolve => {
    const plasmaPixels = plasma(...props);
    resolve(plasmaPixels);
  })
};

export const plasma = (
  width,
  height,
  upperLeftColor,
  upperRightColor,
  downRightColor,
  downLeftColor,
  randomFactor,
  randomFactorReducingRate,
) => {
  const plasma = array2D(width, height);
  const fillRectColors = (
    x,
    y,
    width,
    height,
    upperLeftColor,
    upperRightColor,
    downRightColor,
    downLeftColor,
    randomFactor
  ) => {
    const middleColor = shiftColor(
      averageColor(upperLeftColor, upperRightColor, downLeftColor, downRightColor),
      randomFactor);

    if (width >= 2 || height >= 2) {
      const upperMiddleColor = averageColor(upperLeftColor, upperRightColor);
      const rightMiddleColor = averageColor(upperRightColor, downRightColor);
      const downMiddleColor = averageColor(downRightColor, downLeftColor);
      const leftMiddleColor = averageColor(downLeftColor, upperLeftColor);

      const [leftWidth, rightWidth] = splitInteger(width);
      const [upperHeight, downHeight] = splitInteger(height);

      const leftX = x;
      const upperY = y;
      const rightX = x + leftWidth;
      const downY = y + upperHeight;

      const reducedRandomFactor = randomFactor / randomFactorReducingRate;

      fillRectColors(leftX, upperY, leftWidth, upperHeight,
        upperLeftColor, upperMiddleColor, middleColor, leftMiddleColor, reducedRandomFactor);
      fillRectColors(rightX, upperY, rightWidth, upperHeight,
        upperMiddleColor, upperRightColor, rightMiddleColor, middleColor, reducedRandomFactor);
      fillRectColors(rightX, downY, rightWidth, downHeight,
        middleColor, rightMiddleColor, downRightColor, downMiddleColor, reducedRandomFactor);
      fillRectColors(leftX, downY, leftWidth, downHeight,
        leftMiddleColor, middleColor, downMiddleColor, downLeftColor, reducedRandomFactor);
    } else {
      plasma[x][y] = middleColor;
    }
  };

  fillRectColors(0, 0, width, height,
    upperLeftColor, upperRightColor, downRightColor, downLeftColor, randomFactor);
  return plasma;
};

const averageColor = (...colors) => ({
  r: averageInteger(...colors.map(color => color.r)),
  g: averageInteger(...colors.map(color => color.g)),
  b: averageInteger(...colors.map(color => color.b)),
  a: averageInteger(...colors.map(color => color.a)),
});

const averageInteger = (...nums) => {
  const sum = nums.reduce((sum, val) => sum + val, 0);
  const length = nums.length;
  return Math.floor(sum / length);
};

const splitInteger = (num) => [
  Math.floor(num / 2),
  Math.ceil(num / 2)
];

const shiftColor = (color, randomFactor) => ({
  r: color.r + randomColorShift(randomFactor),
  g: color.g + randomColorShift(randomFactor),
  b: color.b + randomColorShift(randomFactor),
  a: color.a,
});

const randomColorShift = (randomFactor) =>
  Math.floor((0.5 - Math.random()) * randomFactor);