export const rgbToCmyk = ({r, g, b}) => {
  const rp = r/255;
  const gp = g/255;
  const bp = b/255;

  const k = 1 - Math.max(rp, gp, bp);
  const c = (1 - rp - k) / (1 - k);
  const m = (1 - gp - k) / (1 - k);
  const y = (1 - bp - k) / (1 - k);
  return { c, m, y, k};
};