const reflectPoint = point => point.map(coordinate => -coordinate);

const movePoint = (point, vector) => point.map((coordinate, index) => coordinate + vector[index]);

const scalePoint = (point, scale) => point.map(coordinate => coordinate * scale);

const rotatePoint = (point, angle) => {
  const [x, y] = point;
  const sinAngle = Math.sin(angle);
  const cosAngle = Math.cos(angle);

  return [
    x * cosAngle + y * sinAngle,
    -x * sinAngle + y * cosAngle
  ]
};

const transformShape = (shape, transform) => {
  return Object.fromEntries(
    Object.entries(shape)
      .map(([key, value]) => ([key, transform(value)]))
  )
};

export const transformPointWithCenter = (point, center, transform) => {
  const toCenterVector = reflectPoint(center);
  const centeredPoint = movePoint(point, toCenterVector);
  const transformedCenteredPoint = transform(centeredPoint);
  return movePoint(transformedCenteredPoint, center);
};

export const transformShapeWithCenter = (shape, center, transform) => {
  const toCenterVector = reflectPoint(center);
  const centeredShape = transformShape(shape, point => movePoint(point, toCenterVector));
  const transformedCenteredShape = transformShape(centeredShape, transform);
  return transformShape(transformedCenteredShape, point => movePoint(point, center));
};

export const scaleTransformation = scale => point => scalePoint(point, scale);
export const moveTransformation = vector => point => movePoint(point, vector);
export const rotateTransformation = angle => point => rotatePoint(point, angle);

export const combineTransformations = (...transformations) =>
  point => transformations.reduce((resultPoint, transformation) => transformation(resultPoint), point);